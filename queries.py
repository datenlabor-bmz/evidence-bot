default_query = {
    "operationName": "KEYWORD_SEARCH",
    "variables": {
        "data": {
            "from": 0,
            "keyword": "",
            "size": "50",
            "sort_by": "relevance",
            "filters": {
                "product_type": [],
                "sector_name": [],
                "continents": [],
                "threeie_funded": [],
                "threeie_produced": [],
                "fcv_status": [],
                "countries": [],
                "equity_dimension": [],
                "primary_theme": [],
                "equity_focus": [],
                "year_of_publication": [],
                "dataset_available": [],
                "primary_dac_codes": [],
                "un_sdg": [],
                "primary_dataset_availability": [],
                "pre_registration": [],
                "interventions": [],
                "outcome": [],
                "evaluation_method": [],
                "confidence_level": [],
            },
        }
    },
    "query": """query KEYWORD_SEARCH($data: KeywordSearchInput!) {
  keywordSearch(data: $data) {
    search_result {
      product_type
      title
      synopsis
      id
      short_title
      language
      sector_name
      journal
      journal_volume
      journal_issue
      year_of_publication
      publication_type
      publication_url
      grantholding_institution
      evidence_programme
      context
      research_questions
      main_finding
      review_type
      quantitative_method
      qualitative_method
      overall_of_studies
      overall_of_high_quality_studies
      overall_of_medium_quality_studies
      headline_findings
      pages
      evaluation_design
      authors {
        author
        institutions {
          author_affiliation
          department
          author_country
          __typename
        }
        __typename
      }
      continent {
        continent
        countries {
          country
          income_level
          fcv_status
          __typename
        }
        __typename
      }
      project_name {
        project_name
        implementation_agencies {
          implementation_agency
          implement_agency
          __typename
        }
        funding_agencies {
          program_funding_agency
          agency_name
          __typename
        }
        research_funding_agencies {
          research_funding_agency
          agency_name
          __typename
        }
        __typename
      }
      publisher_location
      status
      threeie_funded
      threeie_produced
      is_bookmark
      based_on_the_above_assessments_of_the_methods_how_would_you_rate_the_reliability_of_the_review
      abstract
      open_access
      doi
      equity_focus
      equity_dimension
      equity_description
      keywords
      evaluation_method
      mixed_methods
      unit_of_observation
      methodology
      main_findings
      background
      objectives
      region
      stateprovince_name
      district_name
      citytown_name
      location_name
      study_status
      additional_url {
        additional_url_address
        additional_url
        __typename
      }
      impact_evaluations
      systematic_reviews
      dataset_url
      dataset_available
      __typename
    }
    total_count
    alternative_suggestions {
      text
      __typename
    }
    filters {
      sector_wise_count {
        buckets {
          key
          doc_count
          by_secondary_sector {
            buckets {
              key
              doc_count
              __typename
            }
            __typename
          }
          __typename
        }
        __typename
      }
      continents_wise_count {
        buckets {
          key
          doc_count
          __typename
        }
        __typename
      }
      product_type_wise_count {
        buckets {
          key
          doc_count
          by_secondary_product {
            buckets {
              key
              doc_count
              __typename
            }
            __typename
          }
          __typename
        }
        __typename
      }
      threeie_funded_wise_count {
        buckets {
          key
          doc_count
          __typename
        }
        __typename
      }
      threeie_produced_wise_count {
        buckets {
          key
          doc_count
          __typename
        }
        __typename
      }
      fcv_wise_count {
        buckets {
          key
          doc_count
          __typename
        }
        __typename
      }
      countries_wise_count {
        buckets {
          key
          doc_count
          __typename
        }
        __typename
      }
      equity_focus_wise_count {
        buckets {
          key
          doc_count
          __typename
        }
        __typename
      }
      equity_dimension_wise_count {
        buckets {
          key
          doc_count
          __typename
        }
        __typename
      }
      year_of_publication_wise_count {
        buckets {
          key
          doc_count
          __typename
        }
        __typename
      }
      dataset_available_wise_count {
        buckets {
          key
          doc_count
          __typename
        }
        __typename
      }
      primary_dac_codes_wise_count {
        buckets {
          key
          doc_count
          __typename
        }
        __typename
      }
      un_sdg_wise_count {
        buckets {
          key
          doc_count
          __typename
        }
        __typename
      }
      primary_dataset_availability_wise_count {
        buckets {
          key
          doc_count
          __typename
        }
        __typename
      }
      pre_registration_wise_count {
        buckets {
          key
          doc_count
          __typename
        }
        __typename
      }
      interventions_wise_count {
        buckets {
          key
          doc_count
          __typename
        }
        __typename
      }
      outcome_wise_count {
        buckets {
          key
          doc_count
          __typename
        }
        __typename
      }
      evm_wise_count {
        buckets {
          key
          doc_count
          __typename
        }
        __typename
      }
      themes_wise_count {
        by_primary_theme {
          buckets {
            key
            doc_count
            by_secondary_theme {
              buckets {
                key
                doc_count
                __typename
              }
              __typename
            }
            __typename
          }
          __typename
        }
        __typename
      }
      keywords_wise_count {
        buckets {
          key
          doc_count
          __typename
        }
        __typename
      }
      __typename
    }
    selectedFilters {
      primary_theme
      sector_name
      product_type
      threeie_funded
      threeie_produced
      continents
      countries
      equity_focus
      year_of_publication
      equity_dimension
      fcv_status
      dataset_available
      confidence_level
      __typename
    }
    __typename
  }
}
""",
}

query = {
    "operationName": "KEYWORD_SEARCH",
    "variables": {
        "data": {
            "from": 0,
            "keyword": "",
            "size": "20000",
            "sort_by": "relevance",
            "filters": {},
        }
    },
    "query": """query KEYWORD_SEARCH($data: KeywordSearchInput!) {
  keywordSearch(data: $data) {
    search_result {
      title
      language
      year_of_publication
      publication_type
      publication_url
      abstract
      open_access
      doi
      additional_url {
        additional_url_address
        additional_url
      }
    }
  }
}
""",
}