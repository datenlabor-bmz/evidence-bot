from joblib.memory import Memory
from zenrows import ZenRowsClient
import os
from dotenv import load_dotenv
import requests 
from functools import partial

import litellm

load_dotenv(override=True)
client = ZenRowsClient(os.environ["ZENROWS_API_KEY"])
memory = Memory("cache", verbose=0)


@memory.cache
def get(url, *args, **kwargs):
    try:
        response = requests.get(url, *args, **kwargs)
        response.raise_for_status()
        return response
    except Exception as e:
        client = ZenRowsClient(os.environ["ZENROWS_API_KEY"])
        return client.get(url, *args, **kwargs)


@memory.cache
def post(url, *args, **kwargs):
    try:
        response = requests.post(url, *args, **kwargs)
        response.raise_for_status()
        return response
    except Exception as e:
        client = ZenRowsClient(os.environ["ZENROWS_API_KEY"])
        return client.post(url, *args, **kwargs)

completion = partial(
    memory.cache(litellm.completion),
    model="azure/gpt-4o-quantifier",  # model = azure/<your-azure-deployment-name>
    api_version=os.environ["AZURE_OPENAI_API_VERSION"],
    api_base=os.environ["AZURE_OPENAI_ENDPOINT"],
    api_key=os.environ["AZURE_OPENAI_API_KEY"],
)
