[![Project Status: WIP – Initial development is in progress, but there has not yet been a stable, usable release suitable for the public.](https://www.repostatus.org/badges/latest/wip.svg)](https://www.repostatus.org/#wip)

# evidence-bot

Prototype for a chatbot system for gathering robust scientific evidence on specific issues in development cooperation.

- [Data availability](./data-availability.ipynb)
