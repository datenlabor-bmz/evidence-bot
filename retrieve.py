from util import completion
from textwrap import dedent
import json
import filetype

from pathlib import Path
import random
from tqdm import tqdm
from queries import query
from collections import Counter

from util import get, post
from util import get


def retrieve_fulltext(article, recursion=0):
    if recursion > 3:
        print("recursive depth exceeded")
        return article
    article["fulltext_path"] = None
    url = article.get("alternative_url", article["publication_url"])
    print(article["title"], url)
    try:
        response = get(url)
        response.raise_for_status()
        ext = filetype.guess(response.content)
        ext = ext.extension if ext else "html"
        article["ext"] = ext
        if ext == "html":
            article["html"] = response.text
        fp = f"downloads/{article['year_of_publication']}_{article['title'][:200]}.{ext}"
        with open(fp, "wb") as f:
            f.write(response.content)
        article["fulltext_path"] = fp
    except Exception as e:
        print(e)
        article["has_full_text"] = False
        return article
    if ext == "pdf":
        article["has_full_text"] = True
        return article
    prompt = dedent(
    """
    Does the HTML contain the full text of the article?
    If not, what are the links to the full text?
    Reply with a JSON object of the following schema:
    {
        "has_full_text_reasoning": str, # reasoning for your answer
        "has_full_text": bool, # whether the HTML contains the full text of the article
        "full_text_links_reasoning": str, # reasoning for your answer
        "full_text_links": list[str], # list of links to the full text; give priority to links that directly link to PDFs
    }
    """
    )
    response = completion(
        messages=[
            {"role": "system", "content": prompt},
            {"role": "user", "content": article["html"][:200_000]},
        ],
        response_format={"type": "json_object"},
    ).choices[0]["message"]["content"]
    result = json.loads(response)
    article["has_full_text"] = result["has_full_text"]
    if article["has_full_text"]:
        return article
    if len(result["full_text_links"]) == 0:
        return article
    article["alternative_url"] = result["full_text_links"][0]
    return retrieve_fulltext(article, recursion=recursion + 1)

def retrieve_all():
    response = post("https://api.developmentevidence.3ieimpact.org//graphql", json=query)
    articles = response.json()["data"]["keywordSearch"]["search_result"]

    oa_articles = [
        a
        for a in articles
        if a["open_access"]
        and a["open_access"].lower() == "yes"
        and a["publication_url"]
        and "http" in a["publication_url"]
    ]
    random.seed(0)
    sample_size = 100
    article_sample = random.sample(oa_articles, sample_size)
    Path("downloads").mkdir(exist_ok=True)
    for article in tqdm(article_sample):
        article = retrieve_fulltext(article)
    print(Counter([a["has_full_text"] for a in article_sample]))

retrieve_all()